package com.android.twomodelrecyclerview.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.twomodelrecyclerview.R
import com.android.twomodelrecyclerview.RecyclerViewAdapter
import com.android.twomodelrecyclerview.databinding.ActivityMainBinding
import com.android.twomodelrecyclerview.listener.LecturerListener
import com.android.twomodelrecyclerview.listener.StudentListener
import com.android.twomodelrecyclerview.model.Lecturer
import com.android.twomodelrecyclerview.model.Student

class MainActivity : AppCompatActivity() {
    private lateinit var adapter: RecyclerViewAdapter
    private lateinit var binding: ActivityMainBinding
    private var students = mutableListOf<Student>()
    private var lecturers = mutableListOf<Lecturer>()

    companion object {
        const val STUDENT_REQUEST_CODE = 1
        const val LECTURER_REQUEST_CODE = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        setData()
        adapter = RecyclerViewAdapter(
            students,
            lecturers,
            object : StudentListener {
            override fun studentItemOnClick(position: Int) {
                val intent = Intent(this@MainActivity, StudentProfileInfo::class.java)
                intent.putExtra("clickedStudent", students[position])
                startActivityForResult(intent, STUDENT_REQUEST_CODE)
            }

                override fun studentItemOnLongClickListener(position: Int) {
                    students.removeAt(position)
                    adapter.notifyItemRemoved(position)
                }
            },
            object : LecturerListener {
                override fun lecturerItemOnClick(position: Int) {
                    val intent = Intent(this@MainActivity, LecturerProfileInfo::class.java)
                    intent.putExtra("clickedLecturer", lecturers[position])
                    startActivityForResult(intent, LECTURER_REQUEST_CODE)
                }

                override fun lecturerItemOnLongClickListener(position: Int) {
                    lecturers.removeAt(position)
                    adapter.notifyItemRemoved(students.size + position)
                }
            }
        )
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter


        binding.btnAddStudent.setOnClickListener{
            addStudent()
        }
        binding.btnAddLecturer.setOnClickListener{
            addLecturer()
        }

    }

    private fun addStudent() {
        val intent = Intent(this, NewStudent::class.java)
        intent.putExtra("student", Student())
        startActivityForResult(intent, STUDENT_REQUEST_CODE)
    }

    private fun addLecturer() {
        val intent = Intent(this, NewLecturer::class.java)
        intent.putExtra("lecturer", Lecturer())
        startActivityForResult(intent, LECTURER_REQUEST_CODE)
    }


    private fun setData() {
        students.add(Student(R.drawable.ic_face_mask,"Giorgi","Odishvili","Computer Science","odishvili.giorgi@gmail.com","30/06/2001","Male"))
        students.add(Student(R.drawable.ic_black_cat_face,"Jose ","Robinson","Engineering ","JoseHRobinson@dayrep.com","14/08/2000","Male"))
        students.add(Student(R.drawable.ic_animestyle,"Philipp ","Kappel","Management","PhilippKappel@teleworm.us","28/12/1998","Male"))
        students.add(Student(R.drawable.ic_annoyed_smiley_face,"Petra","Daecher","Computer Science","PetraDaecher@teleworm.us","30/06/2001","Female"))
        lecturers.add(Lecturer(R.drawable.ic_cute_smiley_face,"René","Austerlitz","Intro to Management","ReneAusterlitz@teleworm.us","25/12/1980","Male"))
        lecturers.add(Lecturer(R.drawable.ic_android,"Christian","Abt","Into to Computer Science","ChristianAbt@teleworm.us","20/11/1995","Male"))
        lecturers.add(Lecturer(R.drawable.ic_ice,"Christian","Schäfer","Intro To Physics","ChristianSchafer@dayrep.com","15/10/1992","Male"))
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == STUDENT_REQUEST_CODE && resultCode == RESULT_OK) {
            val user: Student =  data?.getParcelableExtra("newStudent")!!
            intent.extras?.remove("newStudent")
            students.add(user)
            adapter.notifyItemInserted(students.size)
        }else if (requestCode == LECTURER_REQUEST_CODE && resultCode == RESULT_OK) {
            val user: Lecturer =  data?.getParcelableExtra("newLecturer")!!
            intent.extras?.remove("newLecturer")
            lecturers.add(user)
            adapter.notifyItemInserted(students.size + lecturers.size)
        }
    }
}