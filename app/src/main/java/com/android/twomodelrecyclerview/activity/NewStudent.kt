package com.android.twomodelrecyclerview.activity

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.android.twomodelrecyclerview.R
import com.android.twomodelrecyclerview.databinding.ActivityNewStudentBinding
import com.android.twomodelrecyclerview.model.Student
import java.util.*

class NewStudent : AppCompatActivity() {
    private lateinit var picker: DatePickerDialog
    private lateinit var eText: EditText
    private lateinit var user: Student


    private lateinit var binding: ActivityNewStudentBinding


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewStudentBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()


        eText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus)
                showDatePicker()
        }

        eText.setOnClickListener {
            showDatePicker()
        }

        binding.btnSave.setOnClickListener {
            sendUpdatedUser()
        }


    }

    private fun init() {
        eText = binding.edBirthDate
        user = intent.getParcelableExtra("student")!!
        intent.extras?.remove("student")
    }

    @SuppressLint("SetTextI18n")
    private fun showDatePicker() {
        eText.inputType = InputType.TYPE_NULL
        val cldr: Calendar = Calendar.getInstance()
        val day: Int = cldr.get(Calendar.DAY_OF_MONTH)
        val month: Int = cldr.get(Calendar.MONTH)
        val year: Int = cldr.get(Calendar.YEAR)
        // date picker dialog
        picker = DatePickerDialog(
            this@NewStudent,
            { _, year, monthOfYear, dayOfMonth -> eText.setText(dayOfMonth.toString() + "/" + (monthOfYear + 1) + "/" + year) },
            year,
            month,
            day
        )
        picker.show()
    }

    private fun sendUpdatedUser() {
        setData()

        val i = Intent()
        i.putExtra("newStudent", user)
        setResult(RESULT_OK, i)
        finish()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
            user.email = binding.edEmail.text.toString().trim()
            user.firstName = binding.edFirstName.text.toString().trim()
            user.lastName = binding.edLastName.text.toString().trim()
            user.birthDate = binding.edBirthDate.text.toString().trim()
        user.faculty = binding.edSubjectFaculty.text.toString().trim()
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            // Is the button now checked?
            val checked = view.isChecked

            // Check which radio button was clicked
            when (view.getId()) {
                R.id.rb_male ->
                    if (checked) {
                        user.gender = "Male"
                    }
                R.id.rb_female ->
                    if (checked) {
                        user.gender = "Female"

                    }
            }
        }
    }
}