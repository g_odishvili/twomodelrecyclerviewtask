package com.android.twomodelrecyclerview.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.twomodelrecyclerview.R
import com.android.twomodelrecyclerview.databinding.ActivityLecturerProfileInfoBinding
import com.android.twomodelrecyclerview.model.Lecturer

class LecturerProfileInfo : AppCompatActivity() {
    private lateinit var binding: ActivityLecturerProfileInfoBinding
    private lateinit var lecturer: Lecturer


    companion object {
        const val FIRST_NAME = "First Name: "
        const val LAST_NAME = "Last Name: "
        const val EMAIL = "Email: "
        const val GENDER = "Gender: "
        const val DATE_OF_BIRTH = "Date Of Birth: "
        const val SUBJECT = "Subject: "
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLecturerProfileInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        lecturer = intent?.getParcelableExtra("clickedLecturer")!!
        intent.extras?.remove("clickedLecturer")
        binding.ivImage.setImageResource(lecturer.image?: R.drawable.ic_android)
        binding.tvFirstName.text = FIRST_NAME + lecturer.firstName
        binding.tvLastName.text = LAST_NAME + lecturer.lastName
        binding.tvEmail.text = EMAIL + lecturer.email
        binding.tvBirthDate.text = DATE_OF_BIRTH + lecturer.birthDate
        binding.tvGender.text = GENDER + lecturer.gender
        binding.tvSubjectTaught.text = SUBJECT + lecturer.subjectTaught
    }

}