package com.android.twomodelrecyclerview.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.twomodelrecyclerview.R
import com.android.twomodelrecyclerview.databinding.ActivityStudentProfileInfoBinding
import com.android.twomodelrecyclerview.model.Student

class StudentProfileInfo : AppCompatActivity() {
    private lateinit var binding: ActivityStudentProfileInfoBinding
    private lateinit var student: Student


    companion object {
        const val FIRST_NAME = "First Name: "
        const val LAST_NAME = "Last Name: "
        const val EMAIL = "Email: "
        const val GENDER = "Gender: "
        const val DATE_OF_BIRTH = "Date Of Birth: "
        const val STATUS  = "Status: "
        const val FACULTY = "Faculty: "

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStudentProfileInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    @SuppressLint("SetTextI18n")
    private fun init() {
        student = intent?.getParcelableExtra("clickedStudent")!!
        intent.extras?.remove("clickedStudent")
        binding.ivImage.setImageResource(student.image?: R.drawable.ic_android)
        binding.tvFirstName.text = FIRST_NAME + student.firstName
        binding.tvLastName.text = LAST_NAME + student.lastName
        binding.tvEmail.text = EMAIL + student.email
        binding.tvBirthDate.text = DATE_OF_BIRTH + student.birthDate
        binding.tvGender.text = GENDER + student.gender
        binding.tvStatus.text = STATUS + student.status
        binding.tvFaculty.text = FACULTY + student.faculty
    }

}