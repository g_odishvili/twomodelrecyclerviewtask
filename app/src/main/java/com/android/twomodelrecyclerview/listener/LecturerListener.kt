package com.android.twomodelrecyclerview.listener

interface LecturerListener {

    fun lecturerItemOnClick(position: Int)

    fun lecturerItemOnLongClickListener(position: Int)

}