package com.android.twomodelrecyclerview.listener

interface StudentListener {

    fun studentItemOnClick(position: Int)
    fun studentItemOnLongClickListener(position: Int)

}