package com.android.twomodelrecyclerview.model

import android.os.Parcelable
import com.android.twomodelrecyclerview.R
import kotlinx.parcelize.Parcelize

@Parcelize
data class Student(
    var image: Int? = R.drawable.ic_face_mask,
    var firstName: String = "",
    var lastName: String = "",
    var faculty: String="",
    var email: String = "",
    var birthDate: String = "",
    var gender: String = "",
    var status: String = "Active"
): Parcelable
