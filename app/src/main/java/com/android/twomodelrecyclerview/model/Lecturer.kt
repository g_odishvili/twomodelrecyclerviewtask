package com.android.twomodelrecyclerview.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Lecturer(
    var image: Int? = null,
    var firstName: String = "",
    var lastName: String = "",
    var subjectTaught: String = "",
    var email: String = "",
    var birthDate: String = "",
    var gender: String = ""
) : Parcelable
