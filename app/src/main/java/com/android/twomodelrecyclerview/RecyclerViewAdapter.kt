package com.android.twomodelrecyclerview

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.twomodelrecyclerview.databinding.LecturerLayoutBinding
import com.android.twomodelrecyclerview.databinding.StudentLayoutBinding
import com.android.twomodelrecyclerview.listener.LecturerListener
import com.android.twomodelrecyclerview.listener.StudentListener
import com.android.twomodelrecyclerview.model.Lecturer
import com.android.twomodelrecyclerview.model.Student


class RecyclerViewAdapter(
    private val students: MutableList<Student>,
    private val lecturers: MutableList<Lecturer>,
    private val studentListener: StudentListener,
    private val lecturerListener: LecturerListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object{
        private const val LECTURER_VIEW_TYPE = 0
        private const val STUDENT_VIEW_TYPE = 1
        private const val STATUS = "Status: "

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            LECTURER_VIEW_TYPE -> {
                 LecturerViewHolder(
                     LecturerLayoutBinding.inflate(
                         LayoutInflater.from(parent.context),parent, false))
            }
            else -> {
                StudentViewHolder(
                    StudentLayoutBinding.inflate(
                        LayoutInflater.from(parent.context),parent, false))
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is StudentViewHolder -> holder.bind()
            is  LecturerViewHolder-> holder.bind()
        }
    }

    override fun getItemCount(): Int  = students.size + lecturers.size


    inner class StudentViewHolder(private val binding: StudentLayoutBinding)
        : RecyclerView.ViewHolder(binding.root), View.OnClickListener,View.OnLongClickListener{
        private lateinit var student: Student

        @SuppressLint("SetTextI18n")
        fun bind() {
            student = students[adapterPosition]
            binding.ivImage.setImageResource( student.image ?: R.drawable.ic_android)
            binding.tvName.text = student.firstName + " " + student.lastName
            binding.tvPosition.text = "Position: Student"
            binding.tvStatus.text = STATUS + student.status
            binding.root.setOnClickListener(this)
            binding.root.setOnLongClickListener(this)

        }

        override fun onClick(v: View?) {
            studentListener.studentItemOnClick(adapterPosition)
        }

        override fun onLongClick(v: View?): Boolean {
            studentListener.studentItemOnLongClickListener(adapterPosition)
            return true
        }

    }
    inner class LecturerViewHolder(private val binding: LecturerLayoutBinding)
        : RecyclerView.ViewHolder(binding.root), View.OnClickListener,View.OnLongClickListener{
        private lateinit var lecturer: Lecturer

        @SuppressLint("SetTextI18n")
        fun bind() {
            lecturer = lecturers[(adapterPosition - students.size)]
            binding.ivImage.setImageResource( lecturer.image ?: R.drawable.ic_android)
            binding.tvName.text = lecturer.firstName + " " + lecturer.lastName
            binding.tvPosition.text = "Position: Lecturer"
            binding.root.setOnClickListener(this)
            binding.root.setOnLongClickListener(this)

        }

        override fun onClick(v: View?) {
            lecturerListener.lecturerItemOnClick(adapterPosition - students.size)
        }

        override fun onLongClick(v: View?): Boolean {
            lecturerListener.lecturerItemOnLongClickListener(adapterPosition - students.size)
            return true

        }

    }

    override fun getItemViewType(position: Int): Int {
        if (position < students.size) {
            return STUDENT_VIEW_TYPE
        }

        val realPosition = (position - students.size)
        if (realPosition < lecturers.size) {
            return LECTURER_VIEW_TYPE
        }

        return -1
    }
}